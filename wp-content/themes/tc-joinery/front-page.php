<?php get_header(); ?>

<div id="content">
	<div class="container">
	
		<?php get_template_part( 'lib/templates/home/cta' ); ?>

		<?php get_template_part( 'lib/templates/home/services' ); ?>

		<?php get_template_part( 'lib/templates/global/reviews' ); ?>
		
	</div>
</div>

<?php get_footer(); ?>