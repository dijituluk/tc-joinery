<footer>
    <div class="footer-top">
        <div class="container">
            <div class="footer-section"><?php dynamic_sidebar( 'footer-section-1' ) ?></div>
            <div class="footer-section">
                <?php dynamic_sidebar( 'footer-section-2' ) ?>
                <script type="text/JavaScript" src="https://www.freeindex.co.uk/widgets/fiwidget.asp?lid=123730%26tit%3D%26wid%3D200%26agt%3D1%26rak%3D1%26dis%3D0%26wri%3D0%26nrv%3D0%26rts%3DS%26theme%3Ddark"></script>
                <script>
                jQuery(document).ready(function($) {
                    $('footer .footer-section style').remove();

                    if($('.FIBrandBarText').length > 1) {
                        let text = $('.FIBrandBarText:nth-of-type(3)');
                        let stars = $('.FIBrandBarText:nth-of-type(3) + #fi-widget-top-stars');
                        
                        text.remove();
                        
                        stars = stars.detach();
                        stars.appendTo('footer .FIBrandBar');

                        stars.removeAttr('style');
                    }
                });
                </script>
            </div>
            <div class="footer-section"><?php dynamic_sidebar( 'footer-section-3' ) ?></div>
            <div class="footer-section"><?php dynamic_sidebar( 'footer-section-4' ) ?></div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="left">&copy; Copyright <?php echo Date('Y'); ?> Terence Cudworth Joinery Ltd<?php if(is_front_page()) { echo ' - Website maintenance by <a href="https://dijitul.uk">dijitul</a>'; } ?></div>
            <div class="right">
                <?php get_template_part( 'lib/templates/menus/footer' ); ?>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

<style>
.FIW_All{background-color:transparent;font-family:Calibri !important;font-size:14px;font-color:#666 !important;text-decoration:none !important;border:0px !important;padding:3px !important;margin:0px !important;line-height:normal !important;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;}tr.FIW_All{background-color:#FFF !important;}.FIWidgetContainer{border:0px !important;padding:0px !important;text-align:left !important;border-radius:4px 4px 4px 4px !important } .FIBrandBar{background-color:#EBEFF3 !important;border-bottom:1px solid #C9D3DF !important;border-radius:4px 4px 0px 0px !important;padding:12px 10px !important;height:50px !important;}.FIBrandBarBtm{background-color:#EBEFF3 !important;border-top:1px solid #C9D3DF !important;border-radius:0px 0px 4px 4px !important;padding:8px 10px !important;height:45px !important;}.FIBrandBarText{color:#000;background-color:#EBEFF3;font-size:20px;font-weight:bold;float:left;} .FIWidgetInfo{margin:10px 10px 0px 10px !important;padding:0px 0px 6px 0px !important;color:#000 !important;border-bottom:1px solid #F2F6EB !important;}.FIWidgetInfo_review{margin:10px 10px 0px 10px !important;padding:0px 0px 6px 0px !important;font-size:11px;}.FIWidgetInfo_reviewText{margin-left:35px !important;padding-bottom:10px !important;border-bottom:1px solid #F2F6EB !important;}div.rating_image{width:25px;height:25px;background-image:url(https://www.freeindex.co.uk/fx/sprite_ratings_2018.png);background-repeat:no-repeat;border:0px !important;border-radius:50%;}a.FIWidgetLink:link, a.FIWidgetLink:visited{color:#73982B !important;text-decoration:none;font-size:14px;background-color:#fff;}.FIWidget_SnipReview{font-size:12px;color:#54657E;line-height:normal;padding:0px 0px 5px 0px !important;}.FIWidget_FullReview{font-size:12px;color:#54657E;line-height:normal;padding:0px 0px 5px 0px !important;display:none;}.FIWidget_ReviewName{color:#000;font-size:12px;padding:6px 0px 5px 0px !important;}.FIWidget_ReviewMore{color:#390 !important;font-size:12px;margin:5px 0px 5px 0px !important;}.FIWidget_rdist{font-size:12px;color:#54657E;padding:0px;}a.FIWidget_WriteReviewBtn:link, a.FIWidget_WriteReviewBtn:visited {background-color:#390 !important;color:#fff !important;font-size:12px !important;font-weight:bold !important;padding:7px 9px 7px 9px !important;border-radius:3px !important;text-align:center !important;text-decoration:none !important;}.WidgetStars {width:73px;height:13px;padding:0px;margin:3px 0 5px 0px;background-color:#FFF;font-size:10px;}.ratinglarge{width:103px;height:19px;background-image:url(https://www.freeindex.co.uk/fx/sprite_ratings_2018.png);background-repeat:no-repeat;background-color:transparent;}.ratingsmall{width:78px;height:13px;background-image:url(https://www.freeindex.co.uk/fx/sprite_ratings_2018.png);background-repeat:no-repeat;background-color:transparent;}.awardSmall {position:relative;top:2px;width:15px !important;height:22px;background-image:url(https://www.freeindex.co.uk/fx/sprite_ratings_2018.png);background-repeat:no-repeat;margin-right:5px;float:left;background-color:transparent;margin-bottom:10px;}
</style>

</body>
</html>