<?php get_header(); ?>

<?php if(have_posts()) { ?>

	<div id="content">
		<div class="container">

			<?php get_template_part( 'lib/templates/global/sidebar' ); ?>

			<main id="main">
				<?php while(have_posts()) { ?>
					<?php the_post(); ?>
			
					<?php the_content(); ?>
				<?php } ?>
			</main>
		</div>
		
	</div>
	
<?php } ?>

<?php get_footer(); ?>