<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title(' - ', TRUE, 'right'); bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header>
        <div class="container">
            
            <div class="left">
                <div id="logo">
                    <?php get_template_part( 'lib/templates/global/logo' ); ?>
                </div>
            </div>

            <div class="right">
                <div class="contact-info">
                    <?php get_template_part( 'lib/templates/global/social' ); ?>

                    <?php $contact_info = get_field( 'contact_info', 'header' ); ?>

                    <div class="phone">
                        <i class="fas fa-phone"></i><a href="tel:<?php echo str_replace(' ', '', $contact_info['telephone']); ?>"><?php echo $contact_info['telephone']; ?></a>
                    </div>

                    <div class="email">
                        <i class="fas fa-envelope-open"></i><a href="mailto:<?php echo $contact_info['email']; ?>"><?php echo $contact_info['email']; ?></a>
                    </div>
                </div>

                <?php get_template_part( 'lib/templates/menus/header' ); ?>
            </div>

        </div>

        <?php get_template_part( 'lib/templates/global/hero' ); ?>
    </header>