const $ = jQuery

/**
 * Creates a WordpressMenu that can be toggled via css using a class
 */
export class WordpressMenu {
    constructor(selector, toggle) {
        this.menu = selector
        this.toggle = toggle

        this.openClass = 'open'
        this.activeClass = 'active'

        this.setup()
    }

    setup() {
        this.handleMenuToggle()
        this.handleMenuClose()
        this.handleLinkClick()

        this.addDropdownIcon()
    }

    /**
     * Handles toggling the menu, and any extra css toggles
     */
    handleMenuToggle() {
        $(this.toggle).on('click', event => {
            $(this.menu).toggleClass(this.activeClass)

            if(!$(this.menu).hasClass(this.activeClass)) {
                this.closeMenu()
            }
        })
    }

    /**
     * Handles closing the menu if a specific selector is present
     */
    handleMenuClose() {
        $(this.menu).find('.menu-overlay').on('click', event => {
            event.preventDefault()

            this.closeMenu()
        })
    }

    /**
     * Closes the menu, and removes classes from present classToggles
     */
    closeMenu() {
        console.log("Closing menu")
        $(this.menu).removeClass(this.activeClass)

        $(this.menu).find('.menu-item-has-children').children('.sub-menu').slideUp('slow')
    }

    /**
     * Handles the clicking of dropdown menus
     */
    handleLinkClick() {
        $(this.menu).find('.menu-item-has-children > a').click(function(event) {
            var clickedLink = event.target == $(this)[0]

            if(!clickedLink) {
                event.preventDefault()

                $($(this)).parent('.menu-item').toggleClass('open')
                $($(this)).siblings('.sub-menu').slideToggle('slow')
            }
        })
    }

    /**
     * Adds dropdown icons into the menu
     */
    addDropdownIcon() {
        const dropdowns = $(this.menu).find('.menu-item-has-children')

        if(dropdowns.length !== 0) {
            dropdowns.children('a').append('<i class="fas fa-caret-down"></i>')
        }
    }
}