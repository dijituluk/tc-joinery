import "../css/stylesheet.sass"

import { WordpressMenu } from "./WordpressMenu"

const $ = jQuery

$(document).ready(function() {
    const headerMenu = new WordpressMenu(
        "header #header-menu",
        "*[data-toggle='header-menu']"
    )

    // Default Button Transition
    $('.button:not(input)').append('<span></span>')
    
    $('.button:not(input)')
    .on('mouseenter', function(e) {
			var parentOffset = $(this).offset(),
      		relX = e.pageX - parentOffset.left,
      		relY = e.pageY - parentOffset.top;
			$(this).find('span').css({top:relY, left:relX})
    })
    .on('mouseout', function(e) {
			var parentOffset = $(this).offset(),
      		relX = e.pageX - parentOffset.left,
      		relY = e.pageY - parentOffset.top;
    	$(this).find('span').css({top:relY, left:relX})
    });
})