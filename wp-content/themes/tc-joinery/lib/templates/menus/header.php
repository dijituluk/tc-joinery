<div class="header-menu">
	<div class="desktop-border">
		<label data-toggle='header-menu'>
			<div class="hamburger"></div>
		</label>
	
		<div id="header-menu" class="mobile-menu">
			<div class="menu-header">
				<h3>Header Menu</h3>
				<label data-toggle='header-menu'>
					<div class="hamburger active"></div>
				</label>
			</div>
	
			<div class="menu-content">
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'nav' ) ); ?>
			</div>
	
			<div class="menu-footer">
				<?php get_template_part( 'lib/templates/global/social' ); ?>
			</div>
	
			<div class="menu-overlay"></div>
		</div>
	</div>
</div>