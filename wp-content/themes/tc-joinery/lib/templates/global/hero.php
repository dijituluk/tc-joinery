<div id="hero-image" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('<?php echo get_field( 'background_image', 'hero-image' ); ?>');">
	<?php if(is_front_page()) { ?>
		<div class="container">
			<h1 class="line-one"><?php echo get_field( 'line_one', 'hero-image' ); ?></h1>
			<h2 class="line-two"><?php echo get_field( 'line_two', 'hero-image' ); ?></h2>
			<div class="content"><?php echo get_field( 'content', 'hero-image' ); ?></div>

			<?php $buttons = get_field( 'buttons', 'hero-image' ); ?>

			<?php if(count($buttons) > 0) { ?>
				<div class="buttons">
					<?php foreach($buttons as $button) { ?>
						<a class="button" href="<?php echo $button['button_link']; ?>"><?php echo $button['button_text']; ?></a>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
</div>