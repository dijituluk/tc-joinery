<?php

$args = array(
	'post_type' => 'services',
	'meta_query' => array(
		array(
			'key' => 'display_on_homepage',
			'compare' => '=',
			'value' => '1'
		)
	)
);

$query = new WP_Query($args);

if($query->have_posts()) { ?>
	<div id="services" class="break">
		<?php while($query->have_posts()) { ?>
			<div class="service">
				<?php $query->the_post(); ?>
		
				<div class="block description">
					<h2><?php the_title(); ?></h2>
					<div class="service-description"><?php echo get_field( 'service_description' ); ?></div>
					<a href="<?php the_permalink(); ?>" class="button"><?php echo get_field( 'service_button_text' ); ?></a>
				</div>

				<?php if(have_rows( 'service_gallery' )) { ?>
					<?php while(have_rows( 'service_gallery' )) { ?>
						<?php the_row(); ?>
						<div class="block image" style="background: url('<?php echo get_sub_field( 'image' ); ?>');">
							<?php if(get_sub_field( 'description' )) { ?>
								<div class="overlay">
									<div class="overlay-content"><?php echo get_sub_field( 'description' ); ?></div>
								</div>
							<?php } ?>
						</div>
					<?php } ?>
				<?php } ?>

			</div>
		<?php } ?>
	</div>
<?php } ?>