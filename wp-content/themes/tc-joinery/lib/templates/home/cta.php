<section class="cta">
	<div class="left">
		<div class="image">
			<?php $image = wp_get_attachment_image(get_field( 'cta_image', 'home' ), 'full'); ?>
			<?php echo $image; ?>
		</div>
	</div>
	<div class="right">
		<div class="content"><?php echo get_field( 'cta_content', 'home' ); ?></div>
		<div class="buttons">
			<a href="<?php echo get_field( 'cta_button_link', 'home' ); ?>" class="button"><?php echo get_field( 'cta_button_text', 'home' ); ?></a>
		</div>
	</div>
</section>